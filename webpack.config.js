'use strict';
const path = require('path');

const ArcGISPlugin = require("@arcgis/webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require('webpack');

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});


module.exports = {
    mode: 'development',
    devServer: {
        port: 3000,
        open: true,
        proxy: {
            "/parse": "http://localhost:1337"
        }
    },
    module: {
        rules: [
            {
                test: /\.js|.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: "[name]_[local]_[hash:base64]",
                            sourceMap: true,
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /\.less$/,
                use: ["style-loader", {loader: 'css-loader', options: {sourceMap: 1}}, { loader: 'less-loader', options: { javascriptEnabled: true } }]
            }
        ]
    },
    // node: {
    //     process: false,
    //     global: false,
    //     fs: "empty"
    // },
    plugins: [htmlPlugin/*, new ArcGISPlugin({
        // "../app" or similar depending on your build.
        // most likely do not need to change
        root: ".",
        // If you specify locales in the build
        // only those locales are available ar runtime.
        // Leave undefined and all locales will be available at runtime.
        locales: ["en"]
    })*/]
};
