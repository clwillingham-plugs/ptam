import {types, applySnapshot, getSnapshot, flow, resolveIdentifier, getRoot, getType} from "mobx-state-tree"
import _ from 'lodash';
import Parse from "parse";

function sanitize(self, data){
    for(let k in _.cloneDeep(data)){
        if(!data.hasOwnProperty(k)) continue;
        if(data[k].__type === 'Pointer' && data[k].objectId){
            //FIND REF
            console.log('POINTER', data[k].__type);
            data[k] = data[k].objectId;
        }else if(typeof data[k] !== 'string'){
            data[k] = sanitize(data[k]);
        }
    }
    return data;
}

export function createParseStore(_queries) {
    return types
        .model({})
        .actions(self => {
            let subscriptions = {};
            let queries;
            return {
                afterCreate(){
                    queries = _queries;

                },
                add(input, key){
                    // console.log(self[key], key);
                    // let type = getType(self[key]).subType;
                    // let existing_item = resolveIdentifier(type, self[key], input.id);
                    // if(existing_item){
                    //     return update(input, key);
                    // }
                    let item = self[key].push(input.toJSON());
                    self[key][item-1].parseObj = input;
                    // console.log(self[key]);
                    return self[key];
                },
                update(input, key){
                    let type = getType(self[key]).subType;
                    let item = resolveIdentifier(type, self[key], input.id);
                    // console.log(item, input.toJSON());
                    let current_value = _.cloneDeep(getSnapshot(item));
                    applySnapshot(item, _.merge(current_value, input.toJSON()));
                    item.parseObj = input;
                },
                remove(input, key){
                    let type = getType(self[key]).subType;
                    let item = resolveIdentifier(type, self[key], input.id);
                    self[key].remove(item);
                },
                subscribe(input, key) {
                    let query;
                    if(typeof input === "string"){
                        query = new Parse.Query(input);
                    }else if(input instanceof Parse.Query){
                        query = input
                    }else if(input instanceof Object){
                        for(let k in input){
                            if(!input.hasOwnProperty(k)) continue;
                            self.subscribe(input[k], k);
                        }
                    }
                    let subscription = query.subscribe();
                    subscription.on('create', newObj => {
                        self.add(newObj, key);
                    });
                    subscription.on('enter', newObj => {
                        self.add(newObj, key);
                    });
                    subscription.on('update', (obj) => {
                        // console.log('UPDATED', obj.toJSON());
                        self.update(obj, key);
                    });

                    subscription.on('delete', (obj) => {
                        console.log('delete', obj);
                        self.remove(obj, key);
                    })
                    // for(let k in queries) {
                    //     if (!queries.hasOwnProperty(k)) continue;
                    //     let query = queries[k];
                    //     if (subscriptions.hasOwnProperty(k)) continue;
                    //     let subscription = query.subscribe();
                    //     subscription.on('create').then(newObj => {
                    //         console.log(newObj);
                    //     }).catch(error => {
                    //         console.error(error);
                    //     });
                    //     subscriptions[k] = subscription;
                    // }
                },
                fetch: flow (function*(input, key){
                    // let root = getRoot(self);
                    let query;
                    // let parseData = {};
                    // console.log('ORIGINAL', data);
                    if(typeof input === "string"){
                        query = new Parse.Query(input);
                    }else if(input instanceof Parse.Query){
                        query = input
                    }else if(input instanceof Object){
                        for(let k in input){
                            if(!input.hasOwnProperty(k)) continue;
                            yield self.fetch(input[k], k);
                        }
                        return;
                    }
                    // console.log(test);
                    // let data = {};
                    // console.log(query);
                    try{
                        let parseData = (yield query.find());
                        let data = _.cloneDeep(getSnapshot(self));
                        console.log('APPLYING', parseData, 'TO', data);
                        data[key] = parseData.map(obj => obj.toJSON());

                        for(let k in queries){
                            if(!queries.hasOwnProperty(k)) continue;
                            let query = queries[k];
                        }
                        // data = sanitize(self, data);
                        // console.log(data);

                        // console.log('APPLYING', data);
                        applySnapshot(self, data);

                        parseData.forEach(function (obj, i) {
                            // resolveIdentifier(self[key],
                            // if(obj.objectId === self[key][i].objectId) {
                                self[key][i].parseObj = obj;
                            // }
                        });

                    }catch(e){
                        console.log(e)
                    }
                })
            }
        });
}
