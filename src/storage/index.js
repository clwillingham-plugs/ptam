import {types, getPath, tryResolve, getRoot, getType, getParentOfType, hasParentOfType} from "mobx-state-tree";
import {DeviceStore, Device, Sensor} from "./DeviceStore";
import Parse from 'parse';

Parse.initialize('plugsApp', 'plugsMasterKey');
if(window.location.port === '3000'){
    Parse.serverURL = 'http://localhost:1337/parse'; //DEVELOPMENT LAZY WAY

}else{
    Parse.serverURL = window.location.href+'parse'; //PRODUCTION
}
export const data = DeviceStore.create();
// Parse.liveQueryServerURL = 'ws://localhost:1337/parse';

const UIState = types
    .model('UI', {
        selectionPath: types.maybe(types.string),
        highlightedPath: types.maybe(types.string)
    })
    .views(self => ({
        selected(device){
            return (device === self.selectedDevice);
        },
        get selection(){
            return self.selectionPath? tryResolve(data, self.selectionPath) : undefined;
        },
        get highlighted(){
            return self.highlightedPath? tryResolve(data, self.highlightedPath) : undefined;
        },
        get selectedDevice(){
            let selection = self.selection;
            if(selection && getType(selection) === Device){
                return selection
            }else if(selection && hasParentOfType(selection, Device)){
                return getParentOfType(selection, Device);
            }else{
                return undefined;
            }
        }
    }))
    .actions(self => ({
        select(selection) {
            console.log(getRoot(self));
            let _selection;
            if(typeof selection === "string"){
                _selection = tryResolve(data, selection);
            }else{
                _selection = selection;
            }
            try{
                self.selectionPath = getPath(_selection);
            }catch(e){
                self.selectionPath = undefined;
            }
            return true;
        },
        highlight(highlight) {
            let _highlight;
            if(typeof highlight === "string"){
                _highlight = tryResolve(data, highlight);
            }else{
                _highlight = highlight;
            }
            try{
                self.highlightedPath = getPath(_highlight);
            }catch(e){
                self.highlightedPath = undefined;
            }
        }
    }));

export const ui = UIState.create();
// export default {
//     data: DeviceStore.create(),
//     ui: UIState.create()
// };
