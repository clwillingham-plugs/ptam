import {
    types,
    getParent,
    flow,
    getParentOfType,
    getSnapshot,
    applySnapshot,
    resolvePath,
    getRoot,
    resolveIdentifier,
    getPath
} from "mobx-state-tree"
import {createParseStore} from "./ParseStore";
import _ from 'lodash';
import {autorun} from 'mobx';
import Parse from 'parse'

window.Parse = Parse;

function deviceRef(objectId) {
    return {"__type": "Pointer", "className": "Device", "objectId": objectId};
}

export const ParseFile = types.model('image', {
    name: types.string,
    url: types.string
});
const ParseEvent = Parse.Object.extend("Event");
export const Event = types.model('Event', {
    objectId: types.identifier,
    createdAt: types.string,
    updatedAt: types.string,
    device_id: types.reference(types.late(() => Device), {
        get: function (id, self) {
            console.log(parent);
            return resolveIdentifier(Device, getRoot(self).devices, id);
        },
        set: function (value) {
            return value.objectId;
        }
    }),
    image: types.maybe(ParseFile)
    // entered: types.boolean,
    // type: types.string,
    // device: types.string
}).views(self => ({
    get device() {
        return self.device_id;
    }
})).actions(self => ({
    afterCreate() {

    }
}));



export const Location = types.model('Location', {
    objectId: types.identifier,
    lat: types.number,
    lng: types.number,
    createdAt: types.string,
    // device_id: types.string
});
/*.actions(self => ({
    afterCreate(){
        console.log(self.lat, self.lng);
    }
}));*/

export const Sensor = types.model('Sensor', {
    objectId: types.identifier,
    name: types.string,
    type: types.string,
    sample_rate: types.number,
    enabled: types.boolean
    // device: types.late(() => types.reference(Device)),
    // range: types.number,
    // power: types.number,
    // resolution: types.number
})
    .actions((self) => ({
        setEnabled: flow(function* (enabled) {
            self.parseObj.set('enabled', enabled);
            try {
                self.parseObj = yield self.parseObj.save();
                self.enabled = self.parseObj.get('enabled');
            } catch (e) {
                console.log(e);
            }


        }),
        setSampleRate: flow(function* (sample_rate) {
            self.parseObj.set('sample_rate', sample_rate);
            try {
                self.parseObj = yield self.parseObj.save();
                self.sample_rate = self.parseObj.get('sample_rate');
            } catch (e) {
                console.log(e);
            }
        })
    }));

const ParseCommand = Parse.Object.extend("Command");
const ParseAction = Parse.Object.extend('Action');
export const Action = types.compose('Action', createParseStore(), types
    .model('Action', {
        objectId: types.identifier,
        enabled: types.optional(types.boolean, false),
        device_id: types.reference(types.late(() => Device), {
            get: function (id, self) {
                console.log(parent);
                return resolveIdentifier(Device, getRoot(self).devices, id);
            },
            set: function (value) {
                return value.objectId;
            }
        }),
        type: types.string,
    }).actions(self => ({
        execute() {
            console.log('execute called');
            // console.log('DEVICE', self.device);
            let cmd = new ParseCommand();
            cmd.set('action', self.parseObj);
            let event = new ParseEvent();
            event.set('device', self.device_id.parseObj);
            console.log(self.device_id);
            event.set('device_id', self.device_id.objectId);

            cmd.set('event', event);
            cmd.set('device', getParentOfType(self, Device).parseObj);
            cmd.save();
        },
        setEnabled: flow(function* (enabled) {
            self.parseObj.set('enabled', enabled);
            try {
                self.parseObj = yield self.parseObj.save();
                self.enabled = self.parseObj.get('enabled');
            } catch (e) {
                console.log(e);
            }
        })
    })));
//trying to make custom type that handles parse pointers
//{"__type":"Pointer","className":"Action","objectId":"9VbXp8w4zE"}
const ParseRef = function (name, ParseClass, Class) {
    return types.custom({
        name: name,
        fromSnapshot(snapshot) {
            console.log(snapshot);
            return snapshot.objectId;
        },
        toSnapshot(value) {
            let obj = new ParseClass();
            obj.objectId = value;
            return obj;
        },
        isTargetType(something) {
            console.log('isTargetType', something);
            return typeof something === 'string';
        },
        getValidationMessage(options) {
            console.log(options);
            return null;
        }
    })
};

const ParseTrigger = Parse.Object.extend("Trigger");
export const Trigger = types.compose('Trigger', createParseStore(), types
    .model('Trigger', {
        objectId: types.optional(types.identifier, 'new'),
        action_ids: types.optional(types.array(types.reference(Action, {
            get(id, self) {
                return resolveIdentifier(Action, getRoot(self), id);
            },
            set(value, self) {
                return value.objectId;
            }
        })), []),
        type: types.maybe(types.string),
        enabled: types.optional(types.boolean, false)
    })
    .views(self => ({
        get device() {
            return getParentOfType(self, Device);
        },
        // get actions() {
        //     let ids = [];
        //     let root = getRoot(self)
        //     root.devices.forEach(function(device){
        //         console.log(device);
        //         self.action_ids.forEach(function (id) {
        //             let value = resolveIdentifier(Action, device.actions, id);
        //             console.log(value);
        //             if(value){
        //                 // ids.push(value);
        //             }
        //         });
        //     });
        //
        //     return ids;
        // }
    }))
    .actions(self => ({
        save: flow(function* () {
            let data = _.cloneDeep(getSnapshot(self));
            if(data.objectId === 'new'){
                delete data.objectId;
            }
            console.log(data);
            let actions = [];
            data.action_ids.forEach(function (id, i) {
                console.log(id, i);
                let parseAction = new ParseAction();
                parseAction.id = id;
                actions.push(parseAction);
            });
            self.parseObj.set('actions', actions);
            // delete data.action_ids;
            console.log(data.actions);
            console.log(self);
            let parseData = yield self.parseObj.save(data);
            console.log(parseData);
            if(self.objectId !== 'new'){
                applySnapshot(self, parseData.toJSON());
            }else{
                getParentOfType(self, Device).deleteTrigger(self);
            }
            // applySnapshot(self, parseData.toJSON());
        }),
        destroy: flow(function* () {
            return yield self.device.destroyTrigger(self);
        }),
        setEnabled(enabled) {
            self.enabled = enabled;
        },
        setType(type) {
            self.type = type;
        },
        addAction(path) {
            let root = getRoot(self);
            console.log('add', path);
            // console.log(self.parseObj.get('device'));
            self.action_ids.push(resolvePath(root, path));
            console.log(getSnapshot(self));
        },
        removeAction(path) {
            console.log('remove', path);
            let root = getRoot(self);
            console.log('add', path);
            self.action_ids.remove(resolvePath(root, path));

        }
    })));


export const TriggerType = types
    .model('TriggerType', {
        name: types.string,
        args: types.map(types.string)
    });

export const Device = types.compose('Device', createParseStore(), types
    .model('Device', {
        objectId: types.identifier,
        type: types.string,
        name: types.string,
        sensors: types.optional(types.array(Sensor), []),
        actions: types.array(Action),
        triggers: types.array(Trigger),
        events: types.array(Event),
        locations: types.array(Location),
        track_location: types.optional(types.boolean, false),
        trigger_types: types.array(TriggerType)
    })
    .views(self => ({
        get lastLocation() {
            return self.locations[self.locations.length - 1];
        },
        get lastEvent() {
            return self.events[self.events.length - 1];
        }
    }))
    .actions(self => ({
        afterCreate: flow(function* () {
            let LocationQuery = new Parse.Query('Location').equalTo('device', deviceRef(self.objectId));
            let EventQuery = new Parse.Query('Event').equalTo('device', deviceRef(self.objectId));
            let SensorQuery = new Parse.Query('Sensor').equalTo('device', deviceRef(self.objectId));
            let ActionQuery = new Parse.Query('Action').equalTo('device', deviceRef(self.objectId));
            let TriggerQuery = new Parse.Query('Trigger').equalTo('device', deviceRef(self.objectId));
            // {"__type":"Pointer","className":"Device","objectId":self.objectId});
            yield self.fetch(ActionQuery, 'actions');

            yield self.fetch(SensorQuery, 'sensors');
            // console.log("FETCHING TRIGGERS==========================");
            yield self.fetch(TriggerQuery, 'triggers');
            // console.log("FINISHED FETCHING TRIGGERS================");
            self.subscribe(LocationQuery, 'locations');
            self.subscribe(SensorQuery, 'sensors');
            self.subscribe(TriggerQuery, 'triggers');
            self.subscribe(ActionQuery, 'actions');

            // self.subscribe(EventQuery, 'events');

        }),
        cleanup: flow(function*(){
            let LocationQuery = new Parse.Query('Location').equalTo('device', deviceRef(self.objectId));
            self.sensors.forEach((sensor) => {
                sensor.parseObj.destroy();
                self.sensors.remove(sensor);
            });
            self.triggers.forEach((trigger) => {
                trigger.parseObj.destroy();
                self.triggers.remove(trigger);
            });
            let locations = yield LocationQuery.find();
            locations.forEach((location) => {
                location.destroy()
            });
            self.actions.forEach((action) => {
                action.parseObj.destroy();
                self.actions.remove(action);
            });
        }),
        trackLocation: flow(function* (value) {
            self.parseObj.set('track_location', value);
            try {
                self.parseObj = yield self.parseObj.save();
                self.track_location = self.parseObj.get('track_location');
            } catch (e) {
                console.log(e);
            }
        }),
        newTrigger() {
            let parseObj = new ParseTrigger();
            parseObj.set('device', self.parseObj);
            console.log('SELF PARSE OBJ', self.parseObj);
            console.log(self.triggers.push);
            let i = self.triggers.push(parseObj.toJSON()) - 1;
            console.log('MST OBJ', self.triggers[i]);
            let mstObj = self.triggers[i];
            mstObj.parseObj = parseObj;
            return mstObj;
            // return self.add('triggers', parseObj);
        },
        destroyTrigger: flow(function* (trigger) {
            try {
                let parseObj = yield trigger.parseObj.destroy();
                console.log('DELETED', parseObj);
                self.triggers.remove(trigger)
            } catch (e) {
                console.log(e)
            }
        }),
        deleteTrigger(trigger){
            self.triggers.remove(trigger)
        }
    })));

const DeviceBase = types
    .model('DeviceStore', {
        devices: types.array(Device),
        events: types.array(Event),
        sensors: types.array(Sensor),
        // actions: types.array(Action)
        // locations: types.array(Location)
    }).views(self => ({
        get lastEvent() {
            return self.events[self.events.length - 1];
        }
    }))
    .actions(self => ({
        afterCreate: flow(function* () {
            yield self.fetch('Device', 'devices');
            // yield self.fetch('Action', 'actions');
            self.subscribe('Event', 'events');
            self.subscribe('Device', 'devices');
        }),
        destroyDevice: flow(function*(device){
            yield device.cleanup();
            device.parseObj.destroy();
            // self.devices.remove(device);
        })
    }));

export const DeviceStore = types.compose(createParseStore(), DeviceBase);
