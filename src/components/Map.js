import React from "react";
import ReactDOM from 'react-dom'
import esriLoader from "esri-loader";
// import EsriMap from 'esri/map';
// import MapView from 'esri/views/MapView';

export default class Map extends React.Component {
    constructor(props) {
        super(props);
        // this.handleClick = this.handleClick.bind(this);
        this.mapLoaded = this.props.onLoad;
        this.childCallbacks = [];
        this.state = {
            ready: false
        }
    }

    componentDidMount() {
        const mapRef = this.refs.map;
        const self = this;
        this.setState({mapLoaded: false});
        esriLoader.loadModules([
            "esri/Map",
            "esri/views/MapView",])
            .then(([Map, MapView]) => {
                var map = new Map({
                    basemap: 'satellite'
                });
                var view = new MapView({
                    container: mapRef,
                    map: map
                });
                self.setState({
                    view, map, ready: true
                });
                const {children} = this.props;
                let childrenWithProps;
                if(this.state != null){
                    const {map, view} = this.state;
                    childrenWithProps = React.Children.map(children, child =>
                        React.cloneElement(child, {view, map}));
                }
                console.log(childrenWithProps);
                if(!this.childrenNode){
                    this.childrenNode = document.createElement('div');
                }
                ReactDOM.render(childrenWithProps, this.childrenNode);

                if (self.mapLoaded)
                    self.mapLoaded(view, map);
            })
            .catch(err => {
                // handle any errors
                console.error(err);
            });
    }

    render() {

        if(this.state.ready ){

            return (
                <div ref='map' style={{height: '100%', width: '100%'}}/>
            )
        }else{
            return <div ref='map' style={{height: '100%', width: '100%'}}/>
        }

    }
}
