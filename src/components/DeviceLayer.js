import React from "react";
import {loadModules} from 'esri-loader';
import { autorun } from "mobx";
import {ui, data} from '../storage';
export default class DeviceLayer extends React.Component {
    constructor(props) {
        super(props);
        // console.log(props);
        this.state = {
            graphic: null
        };
    }

    setupDevice(device){

    }

    componentDidMount() {
        loadModules(['esri/layers/GraphicsLayer',
            'esri/Graphic',
            'esri/geometry/Point',
            "esri/symbols/SimpleMarkerSymbol"])
            .then(([GraphicsLayer, Graphic, Point, SimpleMarkerSymbol]) => {
                const deviceSymbol = new SimpleMarkerSymbol({color: [0, 255, 255, 0.75]});
                const alertSymbol = new SimpleMarkerSymbol({color: [255, 0, 0, 0.75]});
                const selectionSymbol = new SimpleMarkerSymbol({
                    style: "diamond",
                    outline: {
                        width: 2,
                        color: [0, 92, 230, 1]
                    },
                    size: 22,
                    color: [0, 77, 168, 0]
                });
                const layer = new GraphicsLayer();

                autorun(() => {
                    data.devices.forEach((device) => {
                        console.log('EXECUTED!');
                        // this.setupDevice(device);
                        autorun(() => {
                            let lastLocation = device.lastLocation;
                            let lastEvent = device.lastEvent;
                            if(!lastLocation) return;
                            let {lat, lng} = lastLocation;
                            // console.log(lat, lng);
                            // if(device.locations.length > 0){
                            //     console.log(device.locations[device.locations.length-1]);
                            // }
                            let s = deviceSymbol;
                            console.log(lastEvent);
                            if(lastEvent &&  new Date(lastEvent.createdAt).getTime()+500 > Date.now()){
                                s = alertSymbol;
                            }
                            if(device.g){
                                this.layer.remove(device.g);
                            }
                            let point = new Point(lng, lat);
                            device.g = new Graphic({
                                geometry: point,
                                symbol: s,
                                attributes: {
                                    device: device
                                }});
                            layer.add(device.g);
                            // if(ui.selectionGraphic){
                            //     this.layer.remove(ui.selectionGraphic);
                            // }
                            // if(ui.selected(device)){
                            //     ui.selectionGraphic = new Graphic({
                            //         geometry: point,
                            //         symbol: selectionSymbol,
                            //         attributes: {
                            //             device: device
                            //         }});
                            //     this.setState({lat, lng});
                            // }
                            // if(ui.selection){
                            //     this.layer.add(ui.selectionGraphic);
                            // }
                        })
                    })
                });
                this.props.view.on('click', async (event) => {
                    let response = await this.props.view.hitTest(event);
                    if(response.results.length > 0){
                        let device = response.results[0].graphic.attributes.device;
                        if(device){
                            ui.select(device);
                            // let {lat, lng} = device.lastLocation;
                            // console.log(this.refs.popup);
                            // this.props.view.popup.open({
                            //     title: `Device: ${device.objectId}`,
                            //     location: new Point(lng, lat),
                            //     content: this.refs.popup
                            // });
                        }
                    }else{
                        ui.select(undefined);
                    }
                });
                this.props.map.add(layer);
                this.layer = layer;
                // this.setState({layer, deviceSymbol, alertSymbol});
            });
    }

    render() {
        return (
            <div ref='popup'>
                location {this.state.lat}, {this.state.lng}
            </div>
        )
    }
}
