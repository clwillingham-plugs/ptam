import React from "react";
import {autorun, flow} from "mobx";
import {observer} from 'mobx-react'
import {tryResolve, getType, getPath, hasParentOfType, getParentOfType} from 'mobx-state-tree'
import {Tree, Card, Menu, Dropdown, Icon} from 'antd';
import {Device} from '../storage/DeviceStore';
import {data, ui} from "../storage/index";
import Style from './DeviceListStyle.css'

const TreeNode = Tree.TreeNode;

// const menu = (
//     <Menu>
//         <Menu.Item key="create" onClick={(e)=>console.log('clicked', ui.selection)}>Create Trigger</Menu.Item>
//     </Menu>
// );

function menu(device){
    console.log('DEVICE', device);
    const onClick = function(){
        ui.select(getPath(device.newTrigger()))
    };
    return(
        <Menu>
            <Menu.Item key="create" onClick={(e)=>onClick()}>Create Trigger</Menu.Item>
        </Menu>
    )
}

function dropdown(title, device) {
    return (
        <Dropdown className='ant-dropdown-link' overlay={menu(device)} trigger={['contextMenu']}>
            <span>{title}</span>
        </Dropdown>
    )
}

const DeviceTitle = observer(({device}) => {
    let style = Style.device_item;
    if (device.lastEvent && device.lastEvent.entered) {
        style = Style.active_event;
    }
    return (
        <span className={style}><Icon type='mobile' className={!!device.lastLocation? Style.has_gps : undefined}/> {device.type}: {device.objectId}</span>
    )
});

@observer
export default class DeviceList extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            selection: null
        }
    }

    static sensorList(device) {
        return device.sensors.map((sensor) => <TreeNode title={sensor.name} key={getPath(sensor)}/>)
    }

    static actionList(device) {
        return device.actions.map((action) => <TreeNode title={action.type} key={getPath(action)}/>)
    }

    static triggerList(device) {
        return device.triggers.map((trigger) => <TreeNode title={trigger.type || 'New Trigger'} key={getPath(trigger)}/>);
    }

    componentDidMount(){
        autorun(() => {
            console.log(this.props.ui.selectionPath);
            this.setState({selection: this.props.ui.selectionPath});
        });
    }

    select(key, e) {
        if (key.length === 0) return;
        console.log(key);
        this.setState({selection: key[0]});
        this.props.ui.select(key[0]);
    }

    highlight(key) {
        if (typeof key !== 'string') return;
        this.props.ui.highlight(key);
    }

    static getStyle(device) {
        console.log('STYLE CHANGE!');
        if (device.lastEvent && device.lastEvent.entered) {
            return [Style.active_event, Style.device_item]
        } else {
            return Style.device_item;
        }
    }

    render() {
        let devices = this.props.store.devices;
        let deviceList = devices.map((device) => {

            return (
                <TreeNode title={<DeviceTitle device={device}/>} key={getPath(device)}>

                    <TreeNode title='Sensors' selectable={false}>
                        {DeviceList.sensorList(device)}
                    </TreeNode>
                    <TreeNode title='Actions' selectable={false}>
                        {DeviceList.actionList(device)}
                    </TreeNode>
                    <TreeNode title={dropdown('Triggers', device)} selectable={false}>
                        {DeviceList.triggerList(device)}
                    </TreeNode>
                </TreeNode>
            )
        });
        console.log('SELECTION', this.props.ui.selectionPath);
        return (
            <Tree theme='dark'
                  showLine
                  className={Style.tree}
                  selectedKeys={[this.state.selection]}
                  onSelect={(key, e) => this.select(key, e)}
                  onMouseEnter={({event, node}) => this.highlight(node.props.eventKey)}>
                {deviceList}
            </Tree>
        )
    }
}
