import React from "react";
import DeviceList from "../components/DeviceList";
import Panel from './Panel';
import {data, ui} from "../storage";

const STYLE = {
    height: '100%',
    width: '100%',
    overflow: 'auto'
};

export default class DeviceListPanel extends React.Component {
    render() {
        return (
            <Panel><DeviceList store={data} ui={ui}/></Panel>
        )
    }
}
