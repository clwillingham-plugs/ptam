import React from "react";
import style from './style.css'

export default class Panel extends React.Component {
    render(){
        return (<div className={style.panel}>{this.props.children}</div>)
    }
}
