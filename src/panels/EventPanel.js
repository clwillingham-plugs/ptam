import React from "react";
import {observer} from 'mobx-react';
import {data, ui} from "../storage/index";
import {Timeline, Avatar, Card} from 'antd';
import Style from './style.css';

@observer
export default class EventPanel extends React.Component {

    get timelineItems() {
        console.log('EVENTS', data.events);
        return data.events.map((event) => {
            console.log(event.objectId);
            if (event.image) {
                return (
                    <Timeline.Item>
                        {/*<Avatar src={event.image.url} shape={"square"} size={"large"}/>*/}
                        {/*<a href={event.image.url} target={'_blank'}>*/}
                            {/*{event.createdAt} from {event.device.objectId}: {event.objectId}*/}
                        {/*</a>*/}
                        <Card
                            hoverable
                            style={{ width: 240 }}
                            cover={
                                <img alt="example" src={event.image.url} />
                            }
                        >
                            <Card.Meta
                                title={event.objectId}
                                description={`${event.createdAt}`}
                            />
                        </Card>
                    </Timeline.Item>
                )
            }
            return <Timeline.Item>{event.createdAt} from {event.device.objectId}: {event.objectId}</Timeline.Item>;
        })
    }

    render() {
        return (
            <Timeline className={Style.padded} reverse={true}>
                {this.timelineItems}
            </Timeline>
        )
    }
}
