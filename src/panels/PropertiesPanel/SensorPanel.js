import React from "react";
import Panel from "../Panel/index";
import {observer} from 'mobx-react';
import {data, ui} from "../../storage/index";
import {Form, Input, Checkbox, Radio} from 'antd';
import Style from './style.css';

@observer
export default class SensorPanel extends React.Component {

    setEnabled(e){
        console.log('ONCHANGE', e);
        ui.selection.setEnabled(e.target.checked);
    }

    setSampleRate(e){
        ui.selection.setSampleRate(e.target.value)
    }

    render() {
        console.log(ui.selection);
        return (
            <Form className={Style.form}>
                <Form.Item label="Name"><Input value={ui.selection.name} disabled={true}/></Form.Item>
                <Form.Item label="Sample Rate">
                    <Radio.Group buttonStyle={'solid'}
                                 value={ui.selection.sample_rate}
                                 onChange={(e) => this.setSampleRate(e)}>
                        <Radio.Button value={0}>Fastest</Radio.Button>
                        <Radio.Button value={1}>Game</Radio.Button>
                        <Radio.Button value={3}>Normal</Radio.Button>
                        <Radio.Button value={2}>UI</Radio.Button>
                    </Radio.Group>
                </Form.Item>

                <Checkbox checked={ui.selection.enabled} onChange={(e) => this.setEnabled(e)}>Enabled</Checkbox>
            </Form>
        )
    }
}
