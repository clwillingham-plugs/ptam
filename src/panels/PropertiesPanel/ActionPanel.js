import React from "react";
import Panel from "../Panel/index";
import {observer} from 'mobx-react';
import {data, ui} from "../../storage/index";
import {Form, Input, Checkbox, Radio, Button} from 'antd';
import Style from './style.css';

@observer
export default class ActionPanel extends React.Component {
    setEnabled(e){
        ui.selection.setEnabled(e.target.checked)
    }
    render(){
        return (
            <Form className={Style.form}>
                <Form.Item label={'Name'}>
                    <Input value={ui.selection.type} disabled={true}/>
                </Form.Item>
                <Checkbox value={ui.selection.enabled} onChange={(e) => this.setEnabled(e)}>Enabled</Checkbox>
                <Button type={"danger"} onClick={() => ui.selection.execute()}>Execute</Button>
            </Form>
        )
    }
}
