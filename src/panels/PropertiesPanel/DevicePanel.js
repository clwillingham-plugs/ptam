import React from "react";
import Panel from "../Panel/index";
import {observer} from 'mobx-react';
import {data, ui} from "../../storage/index";
import { Form, Input, Checkbox, Button } from 'antd';
import {autorun} from 'mobx'
import Style from './style.css'

@observer
export default class DevicePanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            device: null
        }
    }
    componentDidMount(){
        autorun(() => {
            console.log('DEVICE PANEL', ui.selectedDevice);
            this.setState({device: ui.selectedDevice})
        })
    }

    trackLocation(e){
        console.log(e.target);
        ui.selection.trackLocation(e.target.checked)
    }
    destroy(){
        data.destroyDevice(ui.selection);
        ui.select(undefined);
    }
    render() {
        return (
            <Form className={Style.form}>
                <Form.Item label={'Name'}>
                    <Input placeholder={ui.selection.name}/>

                </Form.Item>
                <Checkbox checked={ui.selection.track_location} onChange={(e) => this.trackLocation(e)}>
                    Track Location
                </Checkbox>
                <Form.Item>
                    <Button type={"danger"} onClick={() => this.destroy()}>Destroy</Button>
                </Form.Item>
            </Form>
        )
    }
}
