import React from 'react'
import {getType} from 'mobx-state-tree'
import {observer, inject} from 'mobx-react'
import {computed} from 'mobx';
import {ui} from '../../storage/index'
import DevicePanel from "./DevicePanel";
import SensorPanel from "./SensorPanel";
import ActionPanel from "./ActionPanel";
import TriggerPanel from "./TriggerPanel";

@observer
export default class PropertiesPanel extends React.Component {
    constructor(props){
        super(props);
        console.log('PROPS:', props);
    }
    @computed get selectionPath(){
        return ui.selectionPath;
    }
    @computed
    get selectedItem(){
        return ui.selection
    }
    render(){
        console.log(this.selectedItem);
        if(!ui.selection){
            return (
                <p>Please select an item</p>
            )
        }
        switch(getType(ui.selection).name){
            case 'Device':
                return <DevicePanel/>;
            case 'Sensor':
                return <SensorPanel/>;
            case 'Action':
                return <ActionPanel/>;
            case 'Trigger':
                return <TriggerPanel/>;
            default:
                return <p>Please select an item</p>
        }
    }
}
