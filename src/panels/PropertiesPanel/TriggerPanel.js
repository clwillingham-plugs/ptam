import React from "react";
import Panel from "../Panel/index";
import {observer} from 'mobx-react';
import {tryResolve, getType, getPath, hasParentOfType, getParentOfType} from 'mobx-state-tree'
import {data, ui} from "../../storage/index";
import {Form, Input, Checkbox, Radio, Button, Select} from 'antd';
import Style from './style.css';

@observer
export default class TriggerPanel extends React.Component {
    setEnabled(e){
        ui.selection.setEnabled(e.target.checked)
    }
    setType(e){
        console.log(e);
        ui.selection.setType(e);
    }

    destroyTrigger(){
        let trigger = ui.selection;
        ui.select(null);
        trigger.destroy();
    }

    get triggerOptions(){
        let triggerTypes = ui.selection.device.trigger_types;
        console.log(triggerTypes);
        return triggerTypes.map((type) => <Select.Option key={type.name}>{type.name}</Select.Option>)
    }

    save(){
        ui.selection.save();
        if(ui.selection.objectId === 'new'){
            ui.select(undefined);
        }
    }

    buttons(){
        if(ui.selection.objectId){
            return (
                <Form.Item>
                    <Button type={"primary"} onClick={() => ui.selection.save()}>Save</Button>
                    <Button type={"danger"} onClick={() => this.destroyTrigger()}>Delete</Button>
                </Form.Item>
            );
        }else{
            return (
                <Form.Item>
                    <Button type={"primary"} onClick={() => ui.selection.save()}>Create</Button>
                    <Button type={"danger"} onClick={() => this.destroyTrigger()}>Delete</Button>
                </Form.Item>
            )
        }
    }

    actions(){
        console.log('ACTIONS', data.devices.toJSON());
        let options = [];
        data.devices.forEach(function(device){
            // console.log(device.toJSON());
            device.actions.forEach(function(action){
                console.log(action.objectId, action);
                options.push(
                    <Select.Option key={getPath(action)}>{device.objectId + ': ' + action.type}</Select.Option>
                )
            })
        });
        console.log(options);
        return options//data.actions.map((action) => <Select.Option key={getPath(action)}>{action.objectId + ': ' + action.type}</Select.Option>);
    }

    render(){
        return (
            <Form className={Style.form}>
                <Form.Item label={'Type'}>
                    <Select value={ui.selection.type} disabled={false} onSelect={(e) => this.setType(e)}>
                        {this.triggerOptions}
                    </Select>
                </Form.Item>
                <Form.Item label={'Actions'}>
                    <Select
                        value={ui.selection.action_ids.map((action) => getPath(action))}
                        onSelect={(e) => ui.selection.addAction(e)}
                        onDeselect={(e) => ui.selection.removeAction(e)}
                        mode={"multiple"}>
                        {this.actions()}
                    </Select>
                </Form.Item>

                <Checkbox checked={ui.selection.enabled} onChange={(e) => this.setEnabled(e)}>Enabled</Checkbox>
                <Form.Item>
                    {this.buttons()}
                </Form.Item>
            </Form>
        )
    }
}
