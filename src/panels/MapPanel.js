import React from "react";
import Map from "../components/Map";
import DeviceLayer from "../components/DeviceLayer";


export default class MapPanel extends React.Component {
    render(){
        return (
            <Map>
                <DeviceLayer/>
            </Map>
        )
    }
}
