import './style.less'
import React from "react";
import ReactDOM from 'react-dom';
import Map from './components/Map';
import DeviceLayer from './components/DeviceLayer';
import GoldenLayout from 'golden-layout';
import {data, ui} from './storage';
// import DeviceList from "./components/DeviceList";
import DeviceListPanel from "./panels/DeviceListPanel";
import MapPanel from "./panels/MapPanel";
import EventPanel from './panels/EventPanel';
import DevicePanel from "./panels/PropertiesPanel/DevicePanel";
import Index from "./panels/PropertiesPanel/index";


window.React = React;
window.ReactDOM = ReactDOM;

window.data = data;
window.ui = ui;

const layout = new GoldenLayout({
    content: [{
        type: 'column',
        content: [
            {
                type: 'row',
                content: [
                    {
                        type: 'react-component',
                        component: 'device-list',
                        title: 'Devices',
                        width: 20,
                        isClosable: false
                    },
                    {
                        type: 'react-component',
                        component: 'map',
                        title: 'Map',
                        isClosable: false,
                        componentState: {label: 'A'}
                    },
                    {
                        type: 'react-component',
                        width: 20,
                        title: 'Properties',
                        isClosable: false,
                        component: 'properties',
                        props: {ui: ui}
                    }
                ]
            },
            {
                type: 'stack',
                height: 25,
                content: [
                    {
                        type: 'react-component',
                        component: 'event-list',
                        title: 'Events'
                    },
                    {
                        type: 'component',
                        componentName: 'testComponent',
                        componentState: {label: 'Sensor Data will appear here'}
                    }
                ]
            }
        ]
    }]
});

// class MapIndex extends React.Component {
//     render(){
//         return (
//             <Map>
//                 <DeviceLayer/>
//             </Map>
//         )
//     }
// }

// class DeviceListPanel extends React.Component {
//     render() {
//         return (
//             <DeviceList store={data} ui={ui}/>
//         )
//     }
// }

layout.registerComponent('map', MapPanel);
layout.registerComponent('device-list', DeviceListPanel);
layout.registerComponent('event-list', EventPanel)
layout.registerComponent('properties', Index);

layout.registerComponent('testComponent', function (container, componentState) {
    container.getElement().html('<h2>' + componentState.label + '</h2>');
});

layout.init();

// ReactDOM.render(<h1>Hello world</h1>, document.getElementById("index"));
