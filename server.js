const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const ParseDashboard = require('parse-dashboard');
const app = express();
const http = require('http').Server(app);

const api = new ParseServer({
    databaseURI: process.env.MONGODB_URI ||
        'mongodb://localhost:27017/dev', // Connection string for your MongoDB database
    // cloud: '/home/myApp/cloud/main.js', // Absolute path to your Cloud Code
    appId: 'plugsApp',
    masterKey: 'plugsMasterKey', // Keep this key secret!
    fileKey: 'plugsFileKey',
    startLiveQueryServer: true,
    liveQuery: {
        classNames: [
            'Device',
            'Location',
            "Event",
            'Sensor',
            'Action',
            'Command',
            'Trigger'
        ]
    },
    serverURL: 'http://localhost:1337/parse' // Don't forget to change to https if needed
});

var dashboard = new ParseDashboard({
    "apps": [
        {
            "serverURL": "http://localhost:1337/parse",
            "appId": "plugsApp",
            "masterKey": "plugsMasterKey",
            "appName": "PLUGS"
        }
    ]
});

// Serve the Parse API on the /parse URL prefix
app.use('/parse', api);
app.use('/dashboard', dashboard);
app.use(express.static('dist'));

http.listen(process.env.PORT || 1337, function() {
    console.log('parse-server-example running on port 1337.');
});
const parseLiveQueryServer = ParseServer.createLiveQueryServer(http);
